# Web server exemple hisx1 
# Servidor apache interactiu 
FROM debian:latest 
LABEL author="Marco Rizzetto"
LABEL subject="webserver"
RUN apt-get update
RUN apt-get install -y procps iproute2 tree nmap vim apache2
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 80
